# Internship project: SOBOT FARM
โปรเจ็กต์นี้ประกอบด้วย Frontend และ Backend สำหรับจัดการงานและการกำหนดเวลา 

## Features
- มีหน้า Drive, CNC, Transport สำหรับควบคุมมอเตอร์ต่างๆ
- ระบบ Work List ที่ช่วยในการบันทึกและจัดการกับงานที่ต้องทำ
- ระบบปฏิทินแสดงตารางงานและกิจกรรมต่างๆ

## Technologies Used

**Frontend**
- React.js
- Material-UI for styling
- react-big-calendar

**Backend**
- Node.js
- Express.js
- MongoDB for database
- Axios for HTTP requests

## การใช้งาน

**Frontend**
- cd /frontend 
- รันคำสั่ง **npm install** เพื่อติดตั้ง dependencies
- เริ่มต้นโปรเจค Frontend โดยใช้คำสั่ง **npm start** รันที่พอร์ต 3000 

**Backend**
- cd /backend
- รันคำสั่ง **npm install** เพื่อติดตั้ง dependencies
- รัน Backend โดยใช้คำสั่ง **npm run server** รันที่พอร์ต 5000

