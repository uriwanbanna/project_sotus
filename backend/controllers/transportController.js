// src/controllers/transportControl.js

let liftingData = {
    enable: false,
};
let hydraulicData = {
    enable: false,
};


exports.getTransportData = (req, res) => {
    res.json({
        message: 'Lifting Control Data',
        liftingData,
        hydraulicData

    });
};

exports.liftingsystem = (req,res) =>{
    const {enabled} = req.body;
    liftingData = {
        enabled
    }
    console.log(`${enabled}`)
    res.send(`${enabled}`);
}

exports.hydraulicsystem = (req,res) =>{
    const {enabled} = req.body;
    hydraulicData = {
        enabled
    }
    console.log(`${enabled}`)
    res.send(`${enabled}`);
}
