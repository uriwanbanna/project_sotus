// src/controllers/driveControl.js

// เก็บข้อมูล
let speedData = { speedM1: null, speedM2: null, speedM3: null };
let movementData = { direction: null };
// let spraySystemData = {
//     status: false,
//     level: 0,
//     wateringTime: 0,
//     pressure: 0,
//     volume: 0
// };
// let drillingData ={
//     enable: false,
//     depth: 0
// };
// let rotationData ={
//     enable: false,
//     rotationSpeed: 0
// };
// let fertilizerData ={
//     enable: false,
//     fertilizerSpeed: 0
// };
// let seederData ={
//     enable: false,
// };
// let weedingData ={
//     enable: false,
// };

exports.getDriveData = (req, res) => {
    res.json({
        message: 'Drive Control Data',
        movementData,
        // spraySystemData
        // drillingData,
        // rotationData,
        // fertilizerData,
        // seederData,
        // weedingData
    });
};

// exports.postDriveData = (req, res) => {
//     const { speedM1, speedM2, speedM3 } = req.body;
//     speedData = { speedM1, speedM2, speedM3 }; // เก็บค่าที่ได้รับจาก POST request
//     console.log('Received speeds:', 'SpeedM1:', speedM1, 'SpeedM2:', speedM2, 'SpeedM3:', speedM3);
//     res.send('Data received');
// };

exports.moveVehicle = (req, res) => {
    const { direction, speed } = req.body;
    movementData = { direction, speed }; // เก็บค่าที่ได้รับจาก POST request
    console.log('Moving', direction, 'with speed', speed);
    res.send('Movement command received');
};

exports.handleStop = (req, res) => {
    console.log('Stop');
    res.send('Stop command');
};

exports.handleHome = (req, res) => {
    console.log('home position');
    res.send('Home command');
};

// exports.spraySystem = (req, res) => {
//     const { status, level, wateringTime, pressure, volume } = req.body;
//     spraySystemData = {
//         status,
//         level,
//         wateringTime,
//         pressure,
//         volume
//     };

//     console.log(`Received status: ${status}`);
//     console.log(`Level: ${level}, Watering Time: ${wateringTime} minutes, Pressure: ${pressure} bar, Volume: ${volume} cubic/hour`);
//     res.send(`Received status: ${status},Level: ${level}, Watering Time: ${wateringTime} minutes, Pressure: ${pressure} bar, Volume: ${volume} cubic/hour`);

// };

// exports.drillingsystem = (req,res) =>{
//     const { enabled, depth } = req.body;
//     drillingData = {
//         enabled,
//         depth
//     };
//     // Process the drilling system state and depth here
//     console.log(`${enabled}, Depth: ${depth} CM`);
// };

// exports.rotationsystem = (req,res) =>{
//     const {enabled, rotationSpeed} = req.body;
//     rotationData = {
//         enabled,
//         rotationSpeed
//     }

//     console.log(`${enabled}, Speed: ${rotationSpeed} RPM`)

// }

// exports.fertilizersystem = (req,res) =>{
//     const {enabled, fertilizerSpeed} = req.body;
//     fertilizerData = {
//         enabled,
//         fertilizerSpeed
//     }

//     console.log(`${enabled}, Speed: ${fertilizerSpeed} RPM`)

// }

// exports.seedersystem = (req,res) =>{
//     const {enabled} = req.body;
//     seederData = {
//         enabled
//     }
//     console.log(`${enabled}`)
// }

// exports.weedingsystem = (req,res) =>{
//     const {enabled} = req.body;
//     weedingData = {
//         enabled
//     }
//     console.log(`${enabled}`)
// }