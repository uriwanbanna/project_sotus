const Work = require('../models/Work');

exports.getWorks = async (req, res) => {
    try {
        const work = await Work.find();
        res.json(work);
    } catch (error) {
        res.status(500).json({ error: 'An error occurred while fetching work' });
    }
};

exports.addWork = async (req, res) => {
    const { title, start, end } = req.body;

    const newWork = new Work({
        title,
        start,
        end,
    });

    try {
        const savedWork = await newWork.save();
        res.status(201).json(savedWork);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

exports.deleteWork = async (req, res) => {
    const { id } = req.params;

    try {
        await Work.findByIdAndDelete(id);
        res.json({ message: 'Work deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

exports.updateWork = async (req, res) => {
    const { id } = req.params;
    const { title, start, end } = req.body;

    try {
        const updatedWork = await Work.findByIdAndUpdate(
            id,
            { title, start, end },
            { new: true }
        );
        res.json(updatedWork);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

