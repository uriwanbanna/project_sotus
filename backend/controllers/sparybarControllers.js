
let spraySystemData = {
    status: false,
    level: 0,
    wateringTime: 0,
    pressure: 0,
    volume: 0
};

exports.getSpraybarData = (req, res) => {
    res.json({
        message: 'Spraybar Control Data',
        spraySystemData
        
    });
};

exports.spraySystem = (req, res) => {
    const { status, level, wateringTime, pressure, volume } = req.body;
    spraySystemData = {
        status,
        level,
        wateringTime,
        pressure,
        volume
    };

    console.log(`Received status: ${status}`);
    console.log(`Level: ${level}, Watering Time: ${wateringTime} minutes, Pressure: ${pressure} bar, Volume: ${volume} cubic/hour`);
    res.send(`Received status: ${status},Level: ${level}, Watering Time: ${wateringTime} minutes, Pressure: ${pressure} bar, Volume: ${volume} cubic/hour`);

};