// src/controllers/cncControl.js

let drillingData ={
    enable: false,
    depth: 0
};
let rotationData ={
    enable: false,
    rotationSpeed: 0
};
let fertilizerData ={
    enable: false,
    fertilizerSpeed: 0
};
let seederData ={
    enable: false,
};
let weedingData ={
    enable: false,
};

exports.getCNCData = (req, res) => {
    res.json({
        message: 'CNC Control Data',
        drillingData,
        rotationData,
        fertilizerData,
        seederData,
        weedingData
    });
};

exports.drillingsystem = (req,res) =>{
    const { enabled, depth } = req.body;
    drillingData = {
        enabled,
        depth
    };
    // Process the drilling system state and depth here
    console.log(`${enabled}, Depth: ${depth} CM`);
    res.send(`${enabled}, Depth: ${depth} CM`);
};

exports.rotationsystem = (req,res) =>{
    const {enabled, rotationSpeed} = req.body;
    rotationData = {
        enabled,
        rotationSpeed
    }

    console.log(`${enabled}, Speed: ${rotationSpeed} RPM`);
    res.send(`${enabled}, Speed: ${rotationSpeed} RPM`);

}

exports.fertilizersystem = (req,res) =>{
    const {enabled, fertilizerSpeed} = req.body;
    fertilizerData = {
        enabled,
        fertilizerSpeed
    }

    console.log(`${enabled}, Speed: ${fertilizerSpeed} RPM`);
    res.send(`${enabled}, Speed: ${fertilizerSpeed} RPM`);

}

exports.seedersystem = (req,res) =>{
    const {enabled} = req.body;
    seederData = {
        enabled
    }
    console.log(`${enabled}`);
    res.send(`${enabled}`);
}

exports.weedingsystem = (req,res) =>{
    const {enabled} = req.body;
    weedingData = {
        enabled
    }
    console.log(`${enabled}`);
    res.send(`${enabled}`);
}