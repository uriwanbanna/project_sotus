// src/routes/transportControl.js
const express = require('express');
const router = express.Router();
const transportControl = require('../controllers/transportController');

router.get('/', transportControl.getTransportData);

router.post('/liftingsystem', transportControl.liftingsystem);

router.post('/hydraulicsystem', transportControl.hydraulicsystem);

module.exports = router;
