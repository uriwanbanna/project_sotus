// src/routes/cncControl.js
const express = require('express');
const router = express.Router();
const CNCData = require('../controllers/cncController');

router.get('/', CNCData.getCNCData);

router.post('/drillingsystem', CNCData.drillingsystem);

router.post('/rotationsystem', CNCData.rotationsystem);

router.post('/fertilizersystem', CNCData.fertilizersystem);

router.post('/seedersystem', CNCData.seedersystem);

router.post('/weedingsystem', CNCData.weedingsystem);

module.exports = router;
