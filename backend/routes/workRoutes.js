const express = require('express');
const router = express.Router();
const workController = require('../controllers/workController'); // ตรวจสอบว่ามีโมเดลนี้

router.get('/', workController.getWorks);
router.post('/', workController.addWork);
router.put('/:id', workController.updateWork);
router.delete('/:id', workController.deleteWork);

module.exports = router;