const express = require('express');
const router = express.Router();
const Work = require('../models/Work'); // นำเข้าโมเดลของข้อมูลที่ต้องการดึง

// GET /api/home
router.get('/', async (req, res) => {
    try {
        const works = await Work.find(); // ดึงข้อมูลทั้งหมดจากฐานข้อมูล
        res.json(works); // ส่งข้อมูลกลับไปยัง client ในรูปแบบ JSON
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
