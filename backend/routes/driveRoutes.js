// src/routes/driveControl.js
const express = require('express');
const router = express.Router();
const driveControl = require('../controllers/driveController');

// GET request to retrieve drive data
router.get('/', driveControl.getDriveData);

router.post('/move', driveControl.moveVehicle);

router.post('/stop', driveControl.handleStop);

router.post('/home', driveControl.handleHome);

module.exports = router;
