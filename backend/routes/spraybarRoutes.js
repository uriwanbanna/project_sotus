// src/routes/cncControl.js
const express = require('express');
const router = express.Router();
const SparybarData = require('../controllers/sparybarControllers');

router.get('/', SparybarData.getSpraybarData);

router.post('/spraybar', SparybarData.spraySystem);

module.exports = router;
