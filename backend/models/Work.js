const mongoose = require('mongoose');

const workSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 50 // เช่น ต้องมีความยาวไม่เกิน 50 ตัวอักษร
    },
    start: {
        type: Date,
        required: true
    },
    end: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Work', workSchema);
