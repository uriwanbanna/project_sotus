// src/server.js
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 5000;

const driveControlRoutes = require('./routes/driveRoutes');
const cncControlRoutes = require('./routes/cncRoutes');
const transportControlRoutes = require('./routes/transportRoutes');
const spraybarControlRoutes = require('./routes/spraybarRoutes');
const workRoutes = require('./routes/workRoutes');

mongoose.connect('mongodb://localhost:27017/workList', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Connected to MongoDB');
    app.listen(PORT, () => {
        console.log(`Server is running on port ${PORT}`);
    });
}).catch(err => {
    console.error('Error connecting to MongoDB', err);
});

app.use(cors());
app.use(express.json()); // ใช้สำหรับ parse JSON body
app.use('/api/drive-control', driveControlRoutes);
app.use('/api/cnc-control', cncControlRoutes);
app.use('/api/transport-control', transportControlRoutes);
app.use('/api/spraybar-control', spraybarControlRoutes);
app.use('/api/work', workRoutes);


