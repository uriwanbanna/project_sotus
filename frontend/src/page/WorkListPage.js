// WorkPage.js

import React from 'react';
import WorkList from '../components/worklist/WorkList'; // Import WorkList component
import titles from '../components/titles/Title';

const WorkPage = () => {
    const { title, subtitle, style } = titles.WorkList;
    return (
        <div>
            <h1 className="title">{title}</h1>
            <h4 className="subtitle">{subtitle}</h4>
            <WorkList /> {/* Call WorkList component */}
        </div>
    );
};

export default WorkPage;
