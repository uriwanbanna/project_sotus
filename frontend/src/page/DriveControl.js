// src/components/DriveControl.js
import { React, useState } from 'react';
import { SpeedProvider } from '../contexts/SpeedContext';
import SpeedInput from '../components/system/drive_cnc/SpeedInput';
import MovementButtons from '../components/system/drive_cnc/ButtonControl';
import titles from '../components/titles/Title';


const DriveControl = () => {
    const { title, subtitle, style } = titles.driveControl;

    return (
        <div>
            <h1 className="title">{title}</h1>
            <h4 className="subtitle">{subtitle}</h4>

            <SpeedProvider>
                <MovementButtons />
                <SpeedInput />
            </SpeedProvider>
        </div>
    );
};

export default DriveControl;
