import React from 'react';
import titles from '../components/titles/Title';
import VegetablePlotManager from '../components/system/vegetablePlot/VegetablePlotManager'

const VegetablePlot = () => {
    const { title, subtitle} = titles.vegetableControl;

    return (
        <div>
            <h1 className="title">{title}</h1>
            <h4 className="subtitle">{subtitle}</h4>
            <VegetablePlotManager />
        </div>
    );
};

export default VegetablePlot;