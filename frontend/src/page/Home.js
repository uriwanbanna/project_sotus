import React from 'react';
import titles from '../components/titles/Title';
import AllWork from '../components/worklist/AllWorkList'
// import FramBotControl from '../components/system/home/func'


const Home = () => {
    const { title, subtitle} = titles.home;

    return (
        <div>
            <h1 className="title">{title}</h1>
            <h4 className="subtitle">{subtitle}</h4>
            <AllWork/>
            {/* <FramBotControl/> */}
        </div>
    );
};

export default Home;
