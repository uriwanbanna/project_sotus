import React from 'react';
import { SpeedProvider } from '../contexts/SpeedContext';
import SpeedInput from '../components/system/transport/SpeedInput';
import MovementButtons from '../components/system/transport/ButtonControl';
import TransportSystem from '../components/system/TransportSystem';
import titles from '../components/titles/Title';

const TransportControl = () => {
    const { title, subtitle, style } = titles.transportControl;

    return (
        <div>
            <h1 className="title">{title}</h1>
            <h4 className="subtitle">{subtitle}</h4>
            <SpeedProvider>
                <MovementButtons />
                <SpeedInput />
            </SpeedProvider>
            <TransportSystem/>
        </div>
    );
};

export default TransportControl;
