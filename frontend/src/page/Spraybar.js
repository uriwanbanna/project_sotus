import React from 'react';
import titles from '../components/titles/Title';
import { SpeedProvider } from '../contexts/SpeedContext';
import SpeedInput from '../components/system/spraybar/SpeedInput';
import MovementButtons from '../components/system/spraybar/ButtonControl';
import SprayBarSystem from '../components/system/WaterSystem'

const SpraybarControl = () => {

    const { title, subtitle, style } = titles.spraybarControl;

    return (
        <div>
            <h1 className="title">{title}</h1>
            <h4 className="subtitle">{subtitle}</h4>

            <SpeedProvider>
                <MovementButtons />
                <SpeedInput />
            </SpeedProvider>
            <SprayBarSystem/>
        </div>
    );
};

export default SpraybarControl;
