import React from 'react';
import PlantingSystem from '../components/system/PlantingSystem'
import { SpeedProvider } from '../contexts/SpeedContext';
import SpeedInput from '../components/system/drive_cnc/SpeedInput';
import MovementButtons from '../components/system/drive_cnc/ButtonControl';
import titles from '../components/titles/Title';

const CNCControl = () => {
    const { title, subtitle, style } = titles.cncControl;
    return (
        <div>
            <h1 className="title">{title}</h1>
            <h4 className="subtitle">{subtitle}</h4>

            <SpeedProvider>
                <MovementButtons />
                <SpeedInput />
            </SpeedProvider>
            <PlantingSystem/>
        </div>
);
};

export default CNCControl;
