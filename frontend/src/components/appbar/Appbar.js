import React, { useState, useEffect } from 'react';
import { AppBar, Toolbar, Typography, IconButton, Drawer, List, ListItem, ListItemText, Box } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import HomeIcon from '@mui/icons-material/Home';
import { Link } from 'react-router-dom';
import mqtt from 'mqtt';

const Appbar = () => {
    const [openDrawer, setOpenDrawer] = useState(false);
    const [mqttConnected, setMqttConnected] = useState(false);

    useEffect(() => {
        // เชื่อมต่อกับ MQTT broker
        const client = mqtt.connect('ws://test.mosquitto.org:8080'); 

        client.on('connect', () => {
            console.log('Connected to MQTT broker');
            setMqttConnected(true);
        });

        client.on('error', (err) => {
            console.error('Connection error: ', err);
            setMqttConnected(false);
        });

        client.on('close', () => {
            console.log('Disconnected from MQTT broker');
            setMqttConnected(false);
        });

        // ทำความสะอาดเมื่อ component ถูก unmount
        return () => {
            if (client) {
                client.end();
            }
        };
    }, []);

    const handleDrawerOpen = () => {
        setOpenDrawer(true);
    };

    const handleDrawerClose = () => {
        setOpenDrawer(false);
    };

    return (
        <div>
            <AppBar position="static" style={{ backgroundColor: '#013307' }}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu" onClick={handleDrawerOpen}>
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h5" style={{ marginLeft: '10px', marginRight: '10px' }}>
                        SOBOT CONTROL
                    </Typography>
                    <Box flexGrow={1} />
                    <Typography
                        style={{
                            marginRight: '10px',
                            padding: '1px 10px',
                            borderRadius: '10px',
                            backgroundColor: mqttConnected ? 'green' : 'red',
                            color: 'white'
                        }}
                    >
                        {mqttConnected ? 'Connected' : 'Disconnected'}
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                anchor="left"
                open={openDrawer}
                onClose={handleDrawerClose}
            >
                <div
                    role="presentation"
                    onClick={handleDrawerClose}
                    onKeyDown={handleDrawerClose}
                >
                    <List>
                        <ListItem button component={Link} to="/">
                            <ListItemText primary="หน้าหลัก" />
                        </ListItem>
                        <ListItem button component={Link} to="/drive-control">
                            <ListItemText primary="Drive Control" />
                        </ListItem>
                        <ListItem button component={Link} to="/cnc-control">
                            <ListItemText primary="CNC Control" />
                        </ListItem>
                        <ListItem button component={Link} to="/transport-control">
                            <ListItemText primary="Transport Control" />
                        </ListItem>
                        <ListItem button component={Link} to="/spraybar-control">
                            <ListItemText primary="Sparybar Control" />
                        </ListItem>
                        <ListItem button component={Link} to="/vegetable-control">
                            <ListItemText primary="Vegetable Plot" />
                        </ListItem>
                        <ListItem button component={Link} to="/work-list">
                            <ListItemText primary="บันทึกรายการ" />
                        </ListItem>
                    </List>
                </div>
            </Drawer>
        </div>
    );
};

export default Appbar;
