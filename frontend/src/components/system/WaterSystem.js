import React, { useState, useEffect } from 'react';
import axios from 'axios';
import mqtt from 'mqtt';
import FromWaterSystem from '../Froms/FromWaterSystem'
import { Card, TextField, Button, Switch, FormControlLabel, AppBar, Toolbar, IconButton } from '@mui/material';

const WaterSystem = () => {
    const [openSpraybarEditDialog, setOpenSpraybarEditDialog] = useState(false);
    const [spraybarData, setSpraybarData] = useState({ level: 0, wateringTime: 0, pressure: 0, volume: 0 });
    const [spraySystemEnabled, setSpraySystemEnabled] = useState(false);

    // MQTT client state
    const [mqttClient, setMqttClient] = useState(null);

    // Connect to MQTT broker on component mount
    useEffect(() => {
        const brokerUrl = 'ws://test.mosquitto.org:8080'; // Replace with your broker URL
        const client = mqtt.connect(brokerUrl);

        client.on('connect', () => {
            console.log('Connected to MQTT broker');
            setMqttClient(client); // Update state only after successful connection
        });

        client.on('error', (error) => {
            console.error('MQTT connection error:', error);
        });

        return () => client.end(); // Disconnect on component unmount
    }, []);

    const publishToMqtt = (payload) => {
        // If MQTT client is connected, publish the data
        if (mqttClient) {
            const topic = 'spraybarcontrol'; // Replace with your desired topic
            const message = JSON.stringify(payload); // Serialize data
            mqttClient.publish(topic, message);
            console.log('Published data to MQTT:', message);
        } else {
            console.warn('MQTT client not connected, skipping MQTT publish.');
        }
    };

    const handleOpenSpraybarEditDialog = () => {
        setOpenSpraybarEditDialog(true);
    };

    const handleCloseSpraybarEditDialog = () => {
        setOpenSpraybarEditDialog(false);
    };

    const handleSaveSpraybarEditDialog = (data) => {
        setSpraybarData(data);
        handleCloseSpraybarEditDialog();
    };

    const handleSpraySystemToggle = async () => {
        const newSpraySystemEnabled = !spraySystemEnabled;
        setSpraySystemEnabled(newSpraySystemEnabled);

        const payload = {
            status: newSpraySystemEnabled ? 'ระบบรดน้ำ เปิด' : 'ระบบรดน้ำ ปิด',
            level: spraybarData.level,
            wateringTime: spraybarData.wateringTime,
            pressure: spraybarData.pressure,
            volume: spraybarData.volume,
        };

        try {
            const response = await axios.post('http://localhost:5000/api/spraybar-control/spraybar', payload);
            console.log('Response:', response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error:', error);
        }
    };

    return (
        <div className='lg:flex'>
            <div style={{ flex: 1 }}>
                <Card style={{ height: '100%', margin: '20px', display: 'flex', flexDirection: 'column' }}>
                    <AppBar position="static" style={{ backgroundColor: '#70963E' }}>
                        <Toolbar className='text-xl font-bold' style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>ระบบ Spray bar</div>
                            <Button variant="contained" style={{ borderRadius: '10px' }} onClick={handleOpenSpraybarEditDialog}>Edit</Button>
                        </Toolbar>
                    </AppBar>
                    <div className="px-2 py-2" style={{ margin: '15px' }}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: spraySystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {spraySystemEnabled ? 'ระบบรดน้ำ เปิด :' : 'ระบบรดน้ำ ปิด :'}
                            </div>
                            <Switch
                                checked={spraySystemEnabled}
                                onChange={handleSpraySystemToggle}
                            />
                        </div>
                        <div style={{ color: spraySystemEnabled ? 'green' : 'red' }}>
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                ปรับระดับ {spraybarData.level} CM
                            </div>
                            <div style={{ marginTop: '5px', display: 'flex', alignItems: 'center' }}>
                                เวลา {spraybarData.wateringTime} Minutes
                            </div>
                            <div style={{ marginTop: '5px', display: 'flex', alignItems: 'center' }}>
                                Pressure {spraybarData.pressure} Bar
                            </div>
                            <div style={{ marginTop: '5px', display: 'flex', alignItems: 'center' }}>
                                Volume {spraybarData.volume} Cubic/Hour
                            </div>
                        </div>
                        <FromWaterSystem
                            open={openSpraybarEditDialog}
                            onClose={handleCloseSpraybarEditDialog}
                            systemData={spraybarData}
                            onSave={handleSaveSpraybarEditDialog}
                        />
                    </div>
                </Card>
            </div>
        </div>
    );
}

export default WaterSystem;
