import React, { useState, useEffect } from 'react';
import axios from 'axios';
import mqtt from 'mqtt';
import FromPlantingSystem from '../Froms/FromPlantingSystem';
import { Card, Button, Switch, AppBar, Toolbar } from '@mui/material';

const TransportSystem = () => {
    const [liftingSystemEnabled, setLiftingSystemEnabled] = useState(false);
    const [hydraulicSystemEnabled, setHydraulicSystemEnabled] = useState(false);

    // MQTT client state
    const [mqttClient, setMqttClient] = useState(null);

    // Connect to MQTT broker on component mount
    useEffect(() => {
        const brokerUrl = 'ws://test.mosquitto.org:8080'; // Replace with your broker URL
        const client = mqtt.connect(brokerUrl);

        client.on('connect', () => {
            console.log('Connected to MQTT broker');
            setMqttClient(client); // Update state only after successful connection
        });

        client.on('error', (error) => {
            console.error('MQTT connection error:', error);
        });

        return () => client.end(); // Disconnect on component unmount
    }, []);

    const publishToMqtt = (payload) => {
        // If MQTT client is connected, publish the data
        if (mqttClient) {
            const topic = 'systemcontrol'; // Replace with your desired topic
            const message = JSON.stringify(payload); // Serialize data
            mqttClient.publish(topic, message);
            console.log('Published data to MQTT:', message);
        } else {
            console.warn('MQTT client not connected, skipping MQTT publish.');
        }
    };

    const handleLiftingSystemToggle = async () => {
        const newLiftingSystemEnabled = !liftingSystemEnabled;
        setLiftingSystemEnabled(newLiftingSystemEnabled);
        const payload = {
            enabled : newLiftingSystemEnabled ? 'ชุดยกกระบะ เปิด' : 'ชุดยกกระบะ ปิด'
        };
        try {
            const response = await axios.post('http://localhost:5000/api/transport-control/liftingsystem', payload);
            console.log('Response:', response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error updating the lifting system:', error);
        }
    };

    const handleHydraulicSystemToggle = async () => {
        const newHydraulicSystemEnabled = !hydraulicSystemEnabled;
        setHydraulicSystemEnabled(newHydraulicSystemEnabled);
        const payload = {
            enabled: newHydraulicSystemEnabled ? 'ชุดไฮดรอลิก เปิด' : 'ชุดไฮดรอลิก ปิด'
        };
        try {
            const response = await axios.post('http://localhost:5000/api/transport-control/hydraulicsystem', payload);
            console.log('Response:', response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error updating the hydraulic system:', error);
        }
    };

    return (
        <div className='lg:flex'>
            <div style={{ flex: 1 }}>
                <Card style={{ height: '100%', margin: '20px' }}>
                    <AppBar position="static" style={{ backgroundColor: '#70963E' }}>
                        <Toolbar className='text-xl font-bold' style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>Transport lifting System</div>
                        </Toolbar>
                    </AppBar>
                    <div className="px-2 py-2" style={{ margin: '20px' }}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: liftingSystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {liftingSystemEnabled ? 'ชุดยกกระบะ เปิด :' : 'ชุดยกกระบะ ปิด :'}
                            </div>
                            <Switch
                                checked={liftingSystemEnabled}
                                onChange={handleLiftingSystemToggle}
                            />
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: hydraulicSystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {hydraulicSystemEnabled ? 'ชุดไฮดรอลิก เปิด :' : 'ชุดไฮดรอลิก ปิด :'}
                            </div>
                            <Switch
                                checked={hydraulicSystemEnabled}
                                onChange={handleHydraulicSystemToggle}
                            />
                        </div>

                    </div>
                </Card>
            </div>

        </div>
    );

};

export default TransportSystem;