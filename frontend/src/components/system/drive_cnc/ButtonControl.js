// src/components/MovementButtons.js
import React, { useContext, useState, useEffect } from 'react';
import { Button, Card, AppBar, Toolbar } from '@mui/material';
import axios from 'axios';
import DriveControl2 from '../../../assets/image/DriveControl4.svg';
import { SpeedContext } from '../../../contexts/SpeedContext';
import mqtt from 'mqtt'; // Install the mqtt library: npm install mqtt

const MovementButtons = () => {
    const { speedM1, speedM2, speedM3 } = useContext(SpeedContext);

    // MQTT client state
    const [mqttClient, setMqttClient] = useState(null);

    // Connect to MQTT broker on component mount
    useEffect(() => {
        const brokerUrl = 'ws://test.mosquitto.org:8080'; // Replace with your broker URL
        const clientId = 'movement-buttons-client'; // Unique client ID

        const client = mqtt.connect(brokerUrl, { clientId });

        client.on('connect', () => {
            console.log('Connected to MQTT broker');
            setMqttClient(client); // Update state only after successful connection
        });

        client.on('error', (error) => {
            console.error('MQTT connection error:', error);
        });

        return () => client.end(); // Disconnect on component unmount
    }, []);

    const sendMovement = async (direction, speed) => {
        try {
            const response = await axios.post('http://localhost:5000/api/drive-control/move', { direction, speed });
            console.log('Response:', response.data);
            alert(`Moving ${direction} with speed ${speed}`);

            // If MQTT client is connected, publish the movement data
            if (mqttClient) {
                const topic = 'drivecontrol'; // Replace with your desired topic
                const message = JSON.stringify({ direction, speed }); // Serialize data
                mqttClient.publish(topic, message);
                console.log('Published movement data to MQTT:', message);
            } else {
                console.warn('MQTT client not connected, skipping MQTT publish.');
            }
        } catch (error) {
            console.error('There was an error:', error);
        }
    };
    return (
        <Card style={{ margin: '20px' }}>
            <div>
                <AppBar position="static" style={{ backgroundColor: '#70963E' }}>
                    <Toolbar className='text-xl font-bold' >
                        ควบคุมรถ
                    </Toolbar>
                </AppBar>
            </div>
            <div className="flex justify-center">
                <div className="flex flex-col lg:flex-row items-center py-5">
                    <div className="lg:py-20 lg:px-10">
                        <Button
                            title="Control M1 Forward"
                            variant="contained"
                            style={{ marginTop: '20px', marginRight: '20px', borderRadius: '20px' }}
                            color="error"
                            onClick={() => sendMovement('forward', speedM1)}
                        >
                            Forward
                        </Button>
                        <Button
                            title="Control M1 Backward"
                            variant="contained"
                            style={{ marginTop: '20px', marginRight: '20px', borderRadius: '20px' }}
                            color="error"
                            onClick={() => sendMovement('backward', speedM1)}
                        >
                            Backward
                        </Button>
                    </div>
                    <img src={DriveControl2} className="responsive-img lg:w-full" alt="Drive Control" />
                    <div className="py-5 lg:py-20 lg:px-10 flex flex-col items-center justify-center">
                        <Button
                            title="Control M2 Up"
                            variant="contained"
                            style={{ marginTop: '20px', borderRadius: '10px' }}
                            color="success"
                            onClick={() => sendMovement('up', speedM2)} // ใช้ค่า SpeedM1 จาก SpeedInput
                        >
                            Up
                        </Button>
                        <div className="flex justify-center py-5 " style={{ marginTop: '10px', marginBottom: '20px' }}>
                            <Button
                                title="Control M3 Left"
                                variant="contained"
                                className="ml-2 lg:ml-5"
                                style={{ marginTop: '5px', marginRight: '24px', borderRadius: '10px' }}
                                color="amber"
                                onClick={() => sendMovement('left', speedM3)}
                            >
                                Left
                            </Button>
                            <Button
                                title="Control M2 Down"
                                variant="contained"
                                className="mt-5 lg:mt-0"
                                style={{ marginTop: '5px', marginRight: '24px', borderRadius: '10px' }}
                                color="success"
                                onClick={() => sendMovement('down', speedM2)}
                            >
                                Down
                            </Button>
                            <Button
                                title="Control M3 Right"
                                variant="contained"
                                className="ml-5 lg:mr-2"
                                style={{ marginTop: '5px', borderRadius: '10px' }}
                                color="amber"
                                onClick={() => sendMovement('right', speedM3)}
                            >
                                Right
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </Card>
    );
};

export default MovementButtons;
