import React, { useState, useEffect } from 'react';
import axios from 'axios';
import mqtt from 'mqtt';
import FromPlantingSystem from '../Froms/FromPlantingSystem';
import { Card, Button, Switch, AppBar, Toolbar } from '@mui/material';

const PlantingSystem = () => {
    const [openSystemEditDialog, setOpenSystemEditDialog] = useState(false);
    const [systemData, setSystemData] = useState({ depth: 0, rotationSpeed: 0, fertilizerSpeed: 0 });
    const [drillingSystemEnabled, setDrillingSystemEnabled] = useState(false);
    const [rotationSystemEnabled, setRotationSystemEnabled] = useState(false);
    const [fertilizerSystemEnabled, setFertilizerSystemEnabled] = useState(false);
    const [seederSystemEnabled, setSeederSystemEnabled] = useState(false);
    const [weedingSystemEnabled, setWeedingSystemEnabled] = useState(false);

    // MQTT client state
    const [mqttClient, setMqttClient] = useState(null);

    // Connect to MQTT broker on component mount
    useEffect(() => {
        const brokerUrl = 'ws://test.mosquitto.org:8080'; // Replace with your broker URL
        // const clientId = 'planting-system-client'; // Unique client ID

        const client = mqtt.connect(brokerUrl);

        client.on('connect', () => {
            console.log('Connected to MQTT broker');
            setMqttClient(client); // Update state only after successful connection
        });

        client.on('error', (error) => {
            console.error('MQTT connection error:', error);
        });

        return () => client.end(); // Disconnect on component unmount
    }, []);

    const handleOpenSystemEditDialog = () => {
        setOpenSystemEditDialog(true);
    };

    const handleCloseSystemEditDialog = () => {
        setOpenSystemEditDialog(false);
    };

    const handleSaveSystemEditDialog = (data) => {
        setSystemData(data);
        handleCloseSystemEditDialog();
    };

    const publishToMqtt = (payload) => {
        // If MQTT client is connected, publish the data
        if (mqttClient) {
            const topic = 'systemcontrol'; // Replace with your desired topic
            const message = JSON.stringify(payload); // Serialize data
            mqttClient.publish(topic, message);
            console.log('Published data to MQTT:', message);
        } else {
            console.warn('MQTT client not connected, skipping MQTT publish.');
        }
    };

    const handleDrillingSystemToggle = async () => {
        const newDrillingSystemEnabled = !drillingSystemEnabled;
        setDrillingSystemEnabled(newDrillingSystemEnabled);
        const payload = {
            enabled: newDrillingSystemEnabled ? 'ระบบเจาะหลุม เปิด' : 'ระบบเจาะหลุม ปิด',
            depth: systemData.depth
        };
        try {
            const response = await axios.post('http://localhost:5000/api/cnc-control/drillingsystem', payload);
            console.log('Response:',response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error updating the drilling system:', error);
        }
    };

    const handleRotationSystemToggle = async () => {
        const newRotationSystemEnabled = !rotationSystemEnabled;
        setRotationSystemEnabled(newRotationSystemEnabled);
        const payload = {
            enabled: newRotationSystemEnabled ? 'ระบบพรวนดิน เปิด' : 'ระบบพรวนดิน ปิด',
            rotationSpeed: systemData.rotationSpeed
        };
        try {
            const response = await axios.post('http://localhost:5000/api/cnc-control/rotationsystem', payload);
            console.log(response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error updating the rotation system:', error);
        }
    };

    const handleFertilizerSystemToggle = async () => {
        const newFertilizerSystemEnabled = !fertilizerSystemEnabled;
        setFertilizerSystemEnabled(newFertilizerSystemEnabled);
        const payload = {
            enabled: newFertilizerSystemEnabled ? 'ระบบโรยปุ๋ย เปิด' : 'ระบบโรยปุ๋ย ปิด',
            fertilizerSpeed: systemData.fertilizerSpeed
        };
        try {
            const response = await axios.post('http://localhost:5000/api/cnc-control/fertilizersystem', payload);
            console.log(response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error updating the fertilizer system:', error);
        }
    };

    const handleSeederSystemToggle = async () => {
        const newSeederSystemEnabled = !seederSystemEnabled;
        setSeederSystemEnabled(newSeederSystemEnabled);
        const payload = {
            enabled: newSeederSystemEnabled ? 'ระบบหว่านเมล็ด เปิด' : 'ระบบหว่านเมล็ด ปิด'
        };
        try {
            const response = await axios.post('http://localhost:5000/api/cnc-control/seedersystem', payload);
            console.log(response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error updating the seeder system:', error);
        }
    };

    const handleWeedingSystemToggle = async () => {
        const newWeedingSystemEnabled = !weedingSystemEnabled;
        setWeedingSystemEnabled(newWeedingSystemEnabled);
        const payload = {
            enabled: newWeedingSystemEnabled ? 'ระบบกำจัดวัชพืช เปิด' : 'ระบบกำจัดวัชพืช ปิด'
        };
        try {
            const response = await axios.post('http://localhost:5000/api/cnc-control/weedingsystem', payload);
            console.log(response.data);
            publishToMqtt(payload);
        } catch (error) {
            console.error('There was an error updating the weeding system:', error);
        }
    };

    return (
        <div className='lg:flex'>
            <div style={{ flex: 1 }}>
                <Card style={{ height: '100%', margin: '10px' }}>
                    <AppBar position="static" style={{ backgroundColor: '#70963E' }}>
                        <Toolbar className='text-xl font-bold' style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>ระบบการเพาะปลูก</div>
                            <Button variant="contained" style={{ borderRadius: '10px' }} onClick={handleOpenSystemEditDialog}>Edit</Button>
                        </Toolbar>
                    </AppBar>
                    <div className="px-2 py-2" style={{ margin: '20px' }}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: drillingSystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {drillingSystemEnabled ? 'ระบบเจาะหลุม เปิด :' : 'ระบบเจาะหลุม ปิด :'} {systemData.depth} CM
                            </div>
                            <Switch
                                checked={drillingSystemEnabled}
                                onChange={handleDrillingSystemToggle}
                            />
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: rotationSystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {rotationSystemEnabled ? 'ระบบพรวนดิน เปิด :' : 'ระบบพรวนดิน ปิด :'} {systemData.rotationSpeed} RPM
                            </div>
                            <Switch
                                checked={rotationSystemEnabled}
                                onChange={handleRotationSystemToggle}
                            />
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: fertilizerSystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {fertilizerSystemEnabled ? 'ระบบโรยปุ๋ย เปิด :' : 'ระบบโรยปุ๋ย ปิด :'} {systemData.fertilizerSpeed} RPM
                            </div>
                            <Switch
                                checked={fertilizerSystemEnabled}
                                onChange={handleFertilizerSystemToggle}
                            />
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: seederSystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {seederSystemEnabled ? 'ระบบหว่านเมล็ด เปิด :' : 'ระบบหว่านเมล็ด ปิด :'}
                            </div>
                            <Switch
                                checked={seederSystemEnabled}
                                onChange={handleSeederSystemToggle}
                            />
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ color: weedingSystemEnabled ? 'green' : 'red', marginRight: '10px' }}>
                                {weedingSystemEnabled ? 'ระบบกำจัดวัชพืช เปิด :' : 'ระบบกำจัดวัชพืช ปิด :'}
                            </div>
                            <Switch
                                checked={weedingSystemEnabled}
                                onChange={handleWeedingSystemToggle}
                            />
                        </div>
                    </div>
                </Card>
            </div>
            <div style={{ flex: 1 }}>
                <Card style={{ height: '100%', margin: '10px' }}>
                    <FromPlantingSystem open={openSystemEditDialog} onClose={handleCloseSystemEditDialog} onSave={handleSaveSystemEditDialog} />
                </Card>
            </div>
        </div>
    );
};

export default PlantingSystem;
