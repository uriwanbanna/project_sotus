import React, { useState, useEffect } from 'react';
import { Container, Grid, Paper, Button, Typography, Slider, IconButton } from '@mui/material';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import StopIcon from '@mui/icons-material/Stop';
import mqtt from 'mqtt';

const FramBotControl = () => {
    const [mqttConnected, setMqttConnected] = useState(false);
    const [botStatus, setBotStatus] = useState('Idle');
    const [speed, setSpeed] = useState(50);
    const [direction, setDirection] = useState('Forward');
    const [sensorData, setSensorData] = useState({ temperature: 0, humidity: 0 });

    useEffect(() => {
        // เชื่อมต่อกับ MQTT broker
        const client = mqtt.connect('ws://test.mosquitto.org:8080');

        client.on('connect', () => {
            setMqttConnected(true);
            client.subscribe('bot/sensorData');
        });

        client.on('message', (topic, message) => {
            if (topic === 'bot/sensorData') {
                setSensorData(JSON.parse(message.toString()));
            }
        });

        client.on('error', () => setMqttConnected(false));
        client.on('close', () => setMqttConnected(false));

        return () => client.end();
    }, []);

    const startBot = () => setBotStatus('Working');
    const stopBot = () => setBotStatus('Idle');

    const handleSpeedChange = (event, newValue) => setSpeed(newValue);
    const handleDirectionChange = (direction) => setDirection(direction);

    return (
        <Container>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Paper style={{ padding: 16 }}>
                        <Typography variant="h6">Fram Bot Control</Typography>
                        <Typography variant="subtitle1" color={mqttConnected ? 'green' : 'red'}>
                            {mqttConnected ? 'MQTT Connected' : 'MQTT Disconnected'}
                        </Typography>
                    </Paper>
                </Grid>

                <Grid item xs={12} md={6}>
                    <Paper style={{ padding: 16 }}>
                        <Typography variant="h6">Bot Status: {botStatus}</Typography>
                        <Button
                            variant="contained"
                            color="primary"
                            startIcon={<PlayArrowIcon />}
                            onClick={startBot}
                            style={{ marginRight: 8 }}
                        >
                            Start
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            startIcon={<StopIcon />}
                            onClick={stopBot}
                        >
                            Stop
                        </Button>
                    </Paper>
                </Grid>

                <Grid item xs={12} md={6}>
                    <Paper style={{ padding: 16 }}>
                        <Typography variant="h6">Speed Control</Typography>
                        <Slider
                            value={speed}
                            onChange={handleSpeedChange}
                            aria-labelledby="continuous-slider"
                            valueLabelDisplay="auto"
                            min={0}
                            max={100}
                        />
                    </Paper>
                </Grid>

                <Grid item xs={12} md={6}>
                    <Paper style={{ padding: 16 }}>
                        <Typography variant="h6">Direction Control</Typography>
                        <Button variant="contained" onClick={() => handleDirectionChange('Forward')} style={{ marginRight: 8 }}>
                            Forward
                        </Button>
                        <Button variant="contained" onClick={() => handleDirectionChange('Backward')}>
                            Backward
                        </Button>
                    </Paper>
                </Grid>

                <Grid item xs={12} md={6}>
                    <Paper style={{ padding: 16 }}>
                        <Typography variant="h6">Sensor Data</Typography>
                        <Typography>Temperature: {sensorData.temperature}°C</Typography>
                        <Typography>Humidity: {sensorData.humidity}%</Typography>
                    </Paper>
                </Grid>
            </Grid>
        </Container>
    );
};

export default FramBotControl;
