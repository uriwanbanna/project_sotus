import React from 'react';
import { Card, Grid, Typography } from '@mui/material';

const SensorDataCard = ({ title, value }) => (
  <Grid item xs={12} md={4}>
    <Card style={{ padding: 16, margin: '15px', backgroundColor:'#7087FF' }}>
      <Typography>{title}: {value}</Typography>
    </Card>
  </Grid>
);

export default SensorDataCard;
