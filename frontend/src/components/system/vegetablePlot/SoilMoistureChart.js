import React from 'react';
import { Line } from 'react-chartjs-2';

const SoilMoistureChart = ({ data }) => (
  <Line data={data} options={{ maintainAspectRatio: false }} />
);

export default SoilMoistureChart;
