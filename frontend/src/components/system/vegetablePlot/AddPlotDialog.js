import React from 'react';
import { OutlinedInput, TextField, Button, Dialog, DialogTitle, DialogContent, DialogActions, MenuItem, Select, InputLabel, FormControl } from '@mui/material';

const AddPlotDialog = ({ open, onClose, newPlot, handleInputChange, handlePlantTypeChange, addPlot }) => (
  <Dialog open={open} onClose={onClose}>
    <DialogTitle>Add New Plot</DialogTitle>
    <DialogContent>
      <TextField
        label="Plot Name"
        name="name"
        value={newPlot.name}
        onChange={handleInputChange}
        fullWidth
        margin="normal"
      />
      <TextField
        label="Size (sqm)"
        name="size"
        value={newPlot.size}
        onChange={handleInputChange}
        fullWidth
        margin="normal"
      />
      <FormControl fullWidth margin="normal">
        <InputLabel >Plant Type</InputLabel>
        <Select
          label="Plant Type"
          labelId="plant-type-label"
          id="plant-type"
          value={newPlot.plantType}
          onChange={handlePlantTypeChange}
        >
          <MenuItem value="Tomato">Tomato</MenuItem>
          <MenuItem value="Cucumber">Cucumber</MenuItem>
          <MenuItem value="Lettuce">Lettuce</MenuItem>
          <MenuItem value="Carrot">Carrot</MenuItem>
        </Select>
      </FormControl>
      <TextField
        label="Date Planted"
        name="datePlanted"
        type="date"
        value={newPlot.datePlanted}
        onChange={handleInputChange}
        fullWidth
        margin="normal"
        InputLabelProps={{ shrink: true }}
      />
      <TextField
        label="Days to Harvest"
        name="harvestDays"
        type="number"
        value={newPlot.harvestDays}
        onChange={handleInputChange}
        fullWidth
        margin="normal"
        disabled
      />
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose} color="primary">
        Cancel
      </Button>
      <Button onClick={addPlot} color="primary">
        Add
      </Button>
    </DialogActions>
  </Dialog>
);

export default AddPlotDialog;
