import React, { useState, useEffect } from 'react';
import { AppBar, Box, Button, Card, Grid, Toolbar, Typography } from '@mui/material';
import mqtt from 'mqtt';
import SensorDataCard from './SensorDataCard';
import SoilMoistureChart from './SoilMoistureChart';
import AddPlotDialog from './AddPlotDialog';
import PlotCard from './PlotCard';
import ChartJS from './chartConfig';

const VegetablePlot = () => {
  const [mqttConnected, setMqttConnected] = useState(false);
  const [plots, setPlots] = useState([]);
  const [newPlot, setNewPlot] = useState({ name: '', plantType: '', size: '', datePlanted: '', growth: [], harvestDays: '' });
  const [notifications, setNotifications] = useState([]);
  const [sensorData, setSensorData] = useState({});
  const [chartData, setChartData] = useState({
    labels: [],
    datasets: [
      {
        label: 'Soil Moisture',
        data: [],
        borderColor: 'blue',
        fill: false
      }
    ]
  });
  const [openAddDialog, setOpenAddDialog] = useState(false);

  const plantHarvestTimes = {
    Tomato: 75,
    Cucumber: 50,
    Lettuce: 60,
    Carrot: 70
  };

  const generateRandomSensorData = () => {
    return {
      temperature: (Math.random() * 15 + 20).toFixed(1),
      humidity: (Math.random() * 40 + 30).toFixed(1),
      soilMoisture: (Math.random() * 60 + 20).toFixed(1)
    };
  };

  useEffect(() => {
    const client = mqtt.connect('ws://test.mosquitto.org:8080');

    client.on('connect', () => {
      setMqttConnected(true);
      client.subscribe('plot/sensorData');
      client.subscribe('plot/notifications');
    });

    client.on('message', (topic, message) => {
      const msg = message.toString();
      if (topic === 'plot/sensorData') {
        const data = JSON.parse(msg);
        setSensorData(data);
        setChartData(prevState => ({
          ...prevState,
          labels: [...prevState.labels, new Date().toLocaleTimeString()],
          datasets: [{
            ...prevState.datasets[0],
            data: [...prevState.datasets[0].data, data.soilMoisture]
          }]
        }));
      } else if (topic === 'plot/notifications') {
        setNotifications((prevNotifications) => [...prevNotifications, msg]);
        alert(msg);  // Show notification as an alert instead of snackbar
      }
    });

    client.on('error', () => setMqttConnected(false));
    client.on('close', () => setMqttConnected(false));

    const interval = setInterval(() => {
      const data = generateRandomSensorData();
      setSensorData(data);
      setChartData(prevState => ({
        ...prevState,
        labels: [...prevState.labels, new Date().toLocaleTimeString()],
        datasets: [{
          ...prevState.datasets[0],
          data: [...prevState.datasets[0].data, data.soilMoisture]
        }]
      }));
    }, 30000);

    return () => {
      client.end();
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      const now = new Date();
      const soonHarvestPlots = plots.filter(plot => {
        const daysSincePlanted = Math.floor((now - new Date(plot.datePlanted)) / (1000 * 60 * 60 * 24));
        return daysSincePlanted >= plot.harvestDays - 2 && daysSincePlanted <= plot.harvestDays;
      });
      if (soonHarvestPlots.length > 0) {
        const notificationMessage = `Attention! The following plots are nearing harvest time: ${soonHarvestPlots.map(plot => plot.name).join(', ')}`;
        setNotifications(prevNotifications => [...prevNotifications, notificationMessage]);
        alert(notificationMessage);  // Show notification as an alert instead of snackbar
      }
    }, 86400000);

    return () => clearInterval(interval);
  }, [plots]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewPlot((prevPlot) => ({ ...prevPlot, [name]: value }));
  };

  const handlePlantTypeChange = (e) => {
    const plantType = e.target.value;
    const harvestDays = plantHarvestTimes[plantType] || 0;
    setNewPlot((prevPlot) => ({ ...prevPlot, plantType, harvestDays }));
  };

  const addPlot = () => {
    setPlots((prevPlots) => [...prevPlots, { ...newPlot, growth: [] }]);
    setNewPlot({ name: '', plantType: '', size: '', datePlanted: '', growth: [], harvestDays: '' });
    setOpenAddDialog(false);
  };

  const updateGrowth = (index) => {
    const growthUpdate = { date: new Date().toLocaleDateString(), height: (Math.random() * 50).toFixed(1) };
    setPlots((prevPlots) => {
      const updatedPlots = [...prevPlots];
      updatedPlots[index].growth.push(growthUpdate);
      updatedPlots[index].latestGrowth = growthUpdate;
      return updatedPlots;
    });
  };

  const handleDeletePlot = (index) => {
    if (window.confirm('Are you sure you want to delete this plot?')) {
      setPlots((prevPlots) => {
        const updatedPlots = [...prevPlots];
        updatedPlots.splice(index, 1);
        return updatedPlots;
      });
    }
  };

  const generateGrowthChartData = (growthData) => {
    return {
      labels: growthData.map(growth => growth.date),
      datasets: [
        {
          label: 'Plant Growth',
          data: growthData.map(growth => growth.height),
          borderColor: 'green',
          fill: false
        }
      ]
    };
  };

  const calculateDaysSincePlanted = (datePlanted) => {
    const plantedDate = new Date(datePlanted);
    const currentDate = new Date();
    const timeDifference = currentDate - plantedDate;
    const daysDifference = Math.floor(timeDifference / (1000 * 3600 * 24));
    return daysDifference;
  };

  const calculateDaysToHarvest = (datePlanted, harvestDays) => {
    const plantedDate = new Date(datePlanted);
    const currentDate = new Date();
    const timeDifference = currentDate - plantedDate;
    const daysDifference = Math.floor(timeDifference / (1000 * 3600 * 24));
    return harvestDays - daysDifference;
  };

  const [plantingModes, setPlantingModes] = useState(plots.map(() => 'manual')); // Initial mode set to 'manual' for all plots

  const handleModeChange = (index, mode) => {
    const newModes = [...plantingModes];
    newModes[index] = mode;
    setPlantingModes(newModes);
  };

  return (
    <div>
      <Card style={{ margin: '20px' }} >
        <Grid container spacing={3} >
          <SensorDataCard title="Temperature" value={`${sensorData.temperature}°C`}/>
          <SensorDataCard title="Humidity" value={`${sensorData.humidity}%`} />
          <SensorDataCard title="Soil Moisture" value={`${sensorData.soilMoisture}%`} />

          <Grid item xs={12}>
            <Card style={{ padding: 16, margin: '15px' }}>
              <Typography>Soil Moisture Chart</Typography>
              <Box sx={{ height: 400 }}>
                <SoilMoistureChart data={chartData} />
              </Box>
            </Card>
          </Grid>

          <AddPlotDialog
            open={openAddDialog}
            onClose={() => setOpenAddDialog(false)}
            newPlot={newPlot}
            handleInputChange={handleInputChange}
            handlePlantTypeChange={handlePlantTypeChange}
            addPlot={addPlot}
          />
        </Grid>
      </Card>

      <Card style={{ margin: '20px' }}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <AppBar position="static" style={{ backgroundColor: '#70963E' }}>
              <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography>Existing Plots</Typography>
                <Button variant="contained" color="primary" onClick={() => setOpenAddDialog(true)}>
                  Add Plot
                </Button>
              </Toolbar>
            </AppBar>
            <Box style={{ margin: '20px' }} sx={{ display: 'flex', flexWrap: 'wrap', gap: 2 }}>
              {plots.map((plot, index) => (
                <PlotCard
                  key={index}
                  plot={plot}
                  index={index}
                  updateGrowth={updateGrowth}
                  handleDeletePlot={handleDeletePlot}
                  generateGrowthChartData={generateGrowthChartData}
                  calculateDaysToHarvest={calculateDaysToHarvest}
                  calculateDaysSincePlanted={calculateDaysSincePlanted}
                  plantingMode={plantingModes[index]}
                  onModeChange={handleModeChange}
                />
              ))}
            </Box>
          </Grid>
        </Grid>
      </Card>
    </div>
  );
};

export default VegetablePlot;
