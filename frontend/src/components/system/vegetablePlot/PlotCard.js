import React from 'react';
import { Box, Button, Card, List, ListItem, ListItemText, Typography, FormControlLabel, Switch } from '@mui/material';
import { Line } from 'react-chartjs-2';
const PlotCard = ({
    plot,
    index,
    updateGrowth,
    handleDeletePlot,
    generateGrowthChartData,
    calculateDaysToHarvest,
    calculateDaysSincePlanted,
    plantingMode,
    onModeChange
  }) => (
    <Card key={index} sx={{ marginTop: '20px', padding: '16px', maxWidth: 345 }}>
      <List>
        <Box sx={{ backgroundColor: calculateDaysToHarvest(plot.datePlanted, plot.harvestDays) <= 0 ? '#ffcccc' : '#fffacd' }}>
          <Typography variant="body2" color={calculateDaysToHarvest(plot.datePlanted, plot.harvestDays) <= 0 ? "error" : "primary"}>
            {calculateDaysToHarvest(plot.datePlanted, plot.harvestDays) <= 0
              ? `ถึงเวลาเก็บเกี่ยว!`
              : `เหลือเวลา ${calculateDaysToHarvest(plot.datePlanted, plot.harvestDays)} วัน จนถึงเก็บเกี่ยว`}
          </Typography>
        </Box>
        <ListItem sx={{ flexDirection: 'column', alignItems: 'flex-start', paddingBottom: '16px' }}>
          <ListItemText
            primary={plot.name}
            secondary={`ประเภทพืช: ${plot.plantType}, ขนาด: ${plot.size} ตร.ม., วันที่ปลูก: ${calculateDaysSincePlanted(plot.datePlanted)} วัน, วันที่เก็บเกี่ยว: ${plot.harvestDays}`}
          />
          <FormControlLabel
            control={
              <Switch
                checked={plantingMode === 'automatic'}
                onChange={(e) => onModeChange(index, e.target.checked ? 'automatic' : 'manual')}
                color="primary"
              />
            }
            label={plantingMode === 'automatic' ? 'Automatic on' : 'Automatic off'}
            sx={{ marginTop: 2 }}
          />
          {plantingMode === 'automatic' && (
            <Box sx={{ paddingLeft: 2, marginTop: 1 }}>
              <Typography variant="body2" color="textSecondary">
                การปลูกอัตโนมัติ: ระบบจะจัดการการปลูกและเก็บเกี่ยวอัตโนมัติ
              </Typography>
            </Box>
          )}
          {plot.latestGrowth && (
            <Box sx={{ paddingLeft: 2, marginTop: 1 }}>
              <Typography variant="body2" color="textSecondary">
                การเติบโตล่าสุด:
              </Typography>
              <ListItemText
                primary={`วันที่: ${plot.latestGrowth.date}`}
                secondary={`ความสูง: ${plot.latestGrowth.height} ซม.`}
              />
            </Box>
          )}
          <Card sx={{ padding: 2, marginTop: 2, width: '100%' }}>
            <Typography variant="h6">กราฟการเติบโตของ {plot.name}</Typography>
            <Line data={generateGrowthChartData(plot.growth)} />
          </Card>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', marginTop: 2, width: '100%' }}>
            {plantingMode === 'manual' && <Button color="success" onClick={() => updateGrowth(index)}>อัปเดตการเติบโต</Button>}
            <Button color="error" onClick={() => handleDeletePlot(index)}>ลบ</Button>
          </Box>
        </ListItem>
      </List>
    </Card>
  );
  
  export default PlotCard;
  