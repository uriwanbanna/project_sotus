// src/components/SpeedInput.js
import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import mqtt from 'mqtt';
import { TextField, Button, Card, Grid } from '@mui/material';
import StopCircleIcon from '@mui/icons-material/StopCircle';
import HomeIcon from '@mui/icons-material/Home';
import { SpeedContext } from '../../../contexts/SpeedContext';

const SpeedInput = () => {
    const { speedM1, setSpeedM1, speedM2, setSpeedM2, speedM3, setSpeedM3 } = useContext(SpeedContext);

    // MQTT client state
    const [mqttClient, setMqttClient] = useState(null);

    // Connect to MQTT broker on component mount
    useEffect(() => {
        const brokerUrl = 'ws://test.mosquitto.org:8080'; // Replace with your broker URL
        const clientId = 'movement-buttons-client'; // Unique client ID

        const client = mqtt.connect(brokerUrl, { clientId });

        client.on('connect', () => {
            console.log('Connected to MQTT broker');
            setMqttClient(client); // Update state only after successful connection
        });

        client.on('error', (error) => {
            console.error('MQTT connection error:', error);
        });

        return () => client.end(); // Disconnect on component unmount
    }, []);

    const handleStop = async () => {
        try {
            const response = await axios.post('http://localhost:5000/api/drive-control/stop');
            console.log('Stop Response:', response.data);
            alert('Stop command sent');

            // If MQTT client is connected, publish the movement data
            if (mqttClient) {
                const topic = 'drivecontrol'; // Replace with your desired topic
                const message = 'Stop command' 
                mqttClient.publish(topic, message);
                console.log('Published data to MQTT:', message);
            } else {
                console.warn('MQTT client not connected, skipping MQTT publish.');
            }
        } catch (error) {
            console.error('There was an error:', error);
        }
    };

    const handleHome = async () => {
        try {
            const response = await axios.post('http://localhost:5000/api/drive-control/home');
            console.log('Home Response:', response.data);
            alert('Home point command sent');

            // If MQTT client is connected, publish the movement data
            if (mqttClient) {
                const topic = 'drivecontrol'; // Replace with your desired topic
                const message = 'Home point command' 
                mqttClient.publish(topic, message);
                console.log('Published data to MQTT:', message);
            } else {
                console.warn('MQTT client not connected, skipping MQTT publish.');
            }
        } catch (error) {
            console.error('There was an error:', error);
        }
    };

    return (
        <div>
            <Card style={{  margin: '20px' }}>
                <div className="flex flex-col mt-4 py-4 px-8">
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6} md={4}>
                            <h4 style={{ color: '#ff0000' }}>SPEED M1 (mm/s) : {speedM1} %</h4>
                            <TextField
                                type="number"
                                variant="outlined"
                                value={speedM1}
                                onChange={(e) => setSpeedM1(e.target.value)}
                                margin="normal"
                            />
                        </Grid>
                        <Grid item xs={12} sm={6} md={4}>
                            <h4 style={{ color: '#10B61F' }}>SPEED M2 (mm/s) : {speedM2} %</h4>
                            <TextField
                                type="number"
                                variant="outlined"
                                value={speedM2}
                                onChange={(e) => setSpeedM2(e.target.value)}
                                margin="normal"
                            />
                        </Grid>
                        <Grid item xs={12} sm={6} md={4}>
                            <h4 style={{ color: '#FAC832' }}>SPEED M3 (mm/s) : {speedM3} %</h4>
                            <TextField
                                type="number"
                                variant="outlined"
                                value={speedM3}
                                onChange={(e) => setSpeedM3(e.target.value)}
                                margin="normal"
                            />
                        </Grid>
                    </Grid>
                    <div className="flex items-center justify-center mt-4 mb-4 " style={{ marginTop: '10px', marginBottom: '10px' }}>
                        <Button
                            variant="contained"
                            color="error"
                            style={{ marginRight: '10px', borderRadius: '20px' }}
                            onClick={() => handleStop()}
                        >
                            <StopCircleIcon /> <div style={{ marginLeft: '5px' }}>Stop</div>
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            style={{ borderRadius: '20px',backgroundColor: '#3f51b5', color: '#fff', }}
                            onClick={() => handleHome()}
                        >
                            <HomeIcon /> <div style={{ marginLeft: '5px' }}>Home Point</div>
                        </Button>
                    </div>
                </div>
            </Card>
        </div>
    );
};

export default SpeedInput;
