// titles.js


const titles = {
    home: {
        title: 'หน้าหลัก',
        subtitle: 'ยินดีต้อนรับสู่ SOBOT FARM',
    },
    driveControl: {
        title: 'ระบบขับเคลื่อน',
        subtitle: 'Drive Control',
    },
    cncControl: {
        title: 'CNC Control',
        subtitle: '',
    },
    transportControl: {
        title: 'ระบบควบคุมย้ายกระบะแปลงปลูก',
        subtitle: 'Transport Lifting System',
    },
    spraybarControl: {
        title: 'ระบบควบคุมการรดน้ำ',
        subtitle: 'Spraybar System',
    },
    vegetableControl: {
        title: 'ระบบควบคุมแปลงผัก',
        subtitle: 'Vegetable Plot System',
    },
    WorkList: {
        title: 'ระบบบันทึกการทำงาน',
        subtitle: '',
    }
};

export default titles;
