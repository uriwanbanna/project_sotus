import React, { useState } from 'react';
import { Button, Card, AppBar, Toolbar, TextField, Grid, Box } from '@mui/material';

const FormWorklist = ({ addWork }) => {
    const [title, setTitle] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        addWork({
            title: title,
            start: start,
            end: end
        });
        setTitle('');
        setStart('');
        setEnd('');
    };

    return (
        <Card style={{ margin: '5px', padding: '0' }}>
            <AppBar position="static" style={{ backgroundColor: '#70963E' }}>
                <Toolbar style={{ width: '100%', display: 'flex' }}>
                    <h3 className='text-xl font-bold'>เพิ่มรายการทำงาน</h3>
                </Toolbar>
            </AppBar>
            <form onSubmit={handleSubmit} style={{ padding: '10px' }}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <TextField
                            label="หัวข้อ"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                            fullWidth
                            margin="normal"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="Start"
                            type="datetime-local"
                            value={start}
                            onChange={(e) => setStart(e.target.value)}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            label="End"
                            type="datetime-local"
                            value={end}
                            onChange={(e) => setEnd(e.target.value)}
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Box display="flex" justifyContent="flex-end">
                            <Button type="submit" variant="contained" color="primary">Add Work</Button>
                        </Box>
                    </Grid>
                </Grid>
            </form>
        </Card>
    );
};

export default FormWorklist;
