import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField, Grid } from '@mui/material';

export default function FromWaterSystem({ open, onClose, sparybarData, onSave }) {
    const [level, setLevel] = useState(sparybarData?.level || 0);
    const [wateringTime, setWateringTime] = useState(sparybarData?.wateringTime || 0);
    const [pressure, setPressure] = useState(sparybarData?.pressure || 0);
    const [volume, setVolume] = useState(sparybarData?.volume || 0);

    useEffect(() => {
        if (sparybarData) {
            setLevel(sparybarData.level || 0);
            setWateringTime(sparybarData.wateringTime || 0);
            setPressure(sparybarData.pressure || 0);
            setVolume(sparybarData.volume || 0);
        }
    }, [sparybarData]);

    const handleClose = () => {
        onClose();
    };

    const handleLevelChange = (event) => {
        setLevel(event.target.value);
    };

    const handleWateringTimeChange = (event) => {
        setWateringTime(event.target.value);
    };

    const handlePressureChange = (event) => {
        setPressure(event.target.value);
    };

    const handleVolumeChange = (event) => {
        setVolume(event.target.value);
    };

    const handleSaveSpraybar = () => {
        onSave({ level, wateringTime, pressure, volume });
        handleClose();
    };

    return (
        <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm">
            <DialogTitle>Edit: Spray Bar System</DialogTitle>
            <DialogContent>
                <Grid container spacing={2} alignItems="center">
                    <Grid item xs={4}>
                        ปรับระดับ :
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            fullWidth
                            type="number"
                            value={level}
                            onChange={handleLevelChange}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item xs={4}>CM</Grid>

                    <Grid item xs={4}>
                        เวลา :
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            fullWidth
                            type="number"
                            value={wateringTime}
                            onChange={handleWateringTimeChange}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item xs={4}>Minutes</Grid>

                    <Grid item xs={4}>
                        Pressure :
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            fullWidth
                            type="number"
                            value={pressure}
                            onChange={handlePressureChange}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item xs={4}>Bar</Grid>

                    <Grid item xs={4}>
                        Volume :
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            fullWidth
                            type="number"
                            value={volume}
                            onChange={handleVolumeChange}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item xs={4}>Cubic/Hour</Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleSaveSpraybar} variant="contained" color="primary">
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    );
}
