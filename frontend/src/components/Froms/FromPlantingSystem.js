import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField, Grid } from '@mui/material';

export default function FromPlantingSystem({ open, onClose, systemData, onSave }) {
  const [depth, setDepth] = useState(systemData?.depth || 0);
  const [rotationSpeed, setRotationSpeed] = useState(systemData?.rotationSpeed || 0);
  const [fertilizerSpeed, setFertilizerSpeed] = useState(systemData?.fertilizerSpeed || 0);

  useEffect(() => {
    if (systemData) {
      setDepth(systemData.depth || 0);
      setRotationSpeed(systemData.rotationSpeed || 0);
      setFertilizerSpeed(systemData.fertilizerSpeed || 0);
    }
  }, [systemData]);

  const handleClose = () => {
    onClose();
  };

  const handleDepthChange = (event) => {
    setDepth(event.target.value);
  };

  const handleRotationSpeedChange = (event) => {
    setRotationSpeed(event.target.value);
  };

  const handleFertilizerSpeedChange = (event) => {
    setFertilizerSpeed(event.target.value);
  };

  const handleSave = () => {
    onSave({ depth, rotationSpeed, fertilizerSpeed });
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm">
      <DialogTitle>Edit: system</DialogTitle>
      <DialogContent>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={4}>
            ความลึกของหลุม:</Grid>
          <Grid item xs={4}>
            <TextField
              type="number"
              value={depth}
              onChange={handleDepthChange}
              variant="outlined"
              style={{ marginLeft: '10px', marginRight: '10px' }}
            /></Grid>
          <Grid item xs={4}>CM</Grid>
          <Grid item xs={4}>ระบบพรวนดิน :</Grid>
          <Grid item xs={4}>
            <TextField
              type="number"
              value={rotationSpeed}
              onChange={handleRotationSpeedChange}
              variant="outlined"
              style={{ marginLeft: '10px', marginRight: '10px' }}
            /></Grid>
          <Grid item xs={4}>Rotation Speed (RPM)</Grid>
          <Grid item xs={4}>ระบบโรยปุ๋ยเม็ด :</Grid>
          <Grid item xs={4}>
            <TextField
              type="number"
              value={fertilizerSpeed}
              onChange={handleFertilizerSpeedChange}
              variant="outlined"
              style={{ marginLeft: '10px', marginRight: '10px' }}
            /> </Grid>
          <Grid item xs={4}>Fertilizer Speed (RPM)</Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSave} variant="contained" color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}
