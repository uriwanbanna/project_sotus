import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import FormWorklist from '../Froms/FromWorklist';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { Box, Grid, Card, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from '@mui/material';

const localizer = momentLocalizer(moment);

const WorkList = () => {
    const [events, setEvents] = useState([]);
    const [selectedEvent, setSelectedEvent] = useState(null);
    const [editEvent, setEditEvent] = useState({ title: '', start: '', end: '' });
    const [openEditDialog, setOpenEditDialog] = useState(false);

    useEffect(() => {
        const fetchEvents = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/work');
                setEvents(response.data);
            } catch (error) {
                console.error('Error fetching events:', error);
            }
        };

        fetchEvents();
    }, []);


    const addWork = async (newTodo) => {
        try {
            const response = await axios.post('http://localhost:5000/api/work', newTodo);
            setEvents([...events, response.data]);
        } catch (error) {
            console.error('Error adding work:', error);
            alert('Error adding work: ' + error);
        }
    };

    const handleSelectEvent = (event) => {
        setSelectedEvent(event);
        setEditEvent({ title: event.title, start: event.start, end: event.end });
        setOpenEditDialog(true);
    };

    const handleEditChange = (e) => {
        const { name, value } = e.target;
        setEditEvent((prevEvent) => ({ ...prevEvent, [name]: value }));
    };

    const handleEditEvent = async () => {
        try {
            const response = await axios.put(`http://localhost:5000/api/work/${selectedEvent._id}`, editEvent);
            setEvents(events.map(event => (event._id === selectedEvent._id ? response.data : event)));
            setOpenEditDialog(false);
        } catch (error) {
            console.error('Error editing work:', error);
            alert('Error editing work: ' + error);
        }
    };

    const handleDeleteEvent = async () => {
        try {
            await axios.delete(`http://localhost:5000/api/work/${selectedEvent._id}`);
            setEvents(events.filter(event => event._id !== selectedEvent._id));
            setOpenEditDialog(false);
        } catch (error) {
            console.error('Error deleting work:', error);
            alert('Error deleting work: ' + error);
        }
    };


    return (

        <Grid container spacing={4} padding={4}>

            <Grid item xs={12} md={8}>
                <Card style={{ margin: '5px' }}>
                    <Calendar
                        localizer={localizer}
                        events={events}
                        startAccessor="start"
                        endAccessor="end"
                        style={{ height: 500, margin: '20px' }}
                        views={['month', 'agenda']}
                        selectable
                        onSelectEvent={handleSelectEvent}
                    />
                </Card>
            </Grid>
            <Grid item xs={12} md={4}>
                <FormWorklist addWork={addWork} />
            </Grid>
            <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}>
                <DialogTitle>Edit Event</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Edit the details of your event.
                    </DialogContentText>
                    <TextField
                        margin="dense"
                        label="Title"
                        name="title"
                        value={editEvent.title}
                        onChange={handleEditChange}
                        fullWidth
                    />
                    <TextField
                        margin="dense"
                        label="Start"
                        type="datetime-local"
                        name="start"
                        value={editEvent.start}
                        onChange={handleEditChange}
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                    />
                    <TextField
                        margin="dense"
                        label="End"
                        type="datetime-local"
                        name="end"
                        value={editEvent.end}
                        onChange={handleEditChange}
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDeleteEvent} color="secondary">
                        Delete
                    </Button>
                    <Button onClick={() => setOpenEditDialog(false)} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleEditEvent} color="primary">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </Grid>
    );
};

export default WorkList;
