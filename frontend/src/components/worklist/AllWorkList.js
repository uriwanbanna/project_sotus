import React, { useEffect, useState } from 'react';
import axios from 'axios';
import moment from 'moment';
import Plan from '../../assets/image/Vegetable-plot3.svg';
import { Grid, Table, TableBody, TableCell, TableHead, TableRow, Card, AppBar, Toolbar, Tooltip, Menu, MenuItem } from '@mui/material';

const Home = () => {
    const [works, setWorks] = useState([]);
    const [menuPosition, setMenuPosition] = useState({ mouseX: null, mouseY: null });

    useEffect(() => {
        const fetchWorks = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/work');
                setWorks(response.data);
            } catch (error) {
                console.error('Error fetching works:', error);
            }
        };

        fetchWorks();
    }, []);

    const handleMenuClick = (event) => {
        event.preventDefault();
        setMenuPosition({
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
        });
    };

    const handleMenuClose = () => {
        setMenuPosition({ mouseX: null, mouseY: null });
    };

    return (
        <div>
            <Grid container spacing={4} padding={4}>
                <Grid item xs={12} md={6}>
                    <Card sx={{ margin: '5px' }}>
                        <MenuItem onClick={handleMenuClose}>ตำแหน่ง x: {menuPosition.mouseX}, y: {menuPosition.mouseY}</MenuItem>
                        <Tooltip >
                            <img src={Plan} alt="Plan" style={{ width: '100%', height: 'auto', cursor: 'pointer' }} onClick={handleMenuClick} />
                        </Tooltip>
                        <Menu
                            open={menuPosition.mouseY !== null}
                            onClose={handleMenuClose}
                            anchorReference="anchorPosition"
                            anchorPosition={
                                menuPosition.mouseY !== null && menuPosition.mouseX !== null
                                    ? { top: menuPosition.mouseY, left: menuPosition.mouseX }
                                    : undefined
                            }
                        >
                            
                            <MenuItem onClick={handleMenuClose}>รดน้ำต้นไม้</MenuItem>
                            <MenuItem onClick={handleMenuClose}>พรวนดิน</MenuItem>
                            <MenuItem onClick={handleMenuClose}>หว่านเมล็ด</MenuItem>
                        </Menu>
                    </Card>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Card sx={{ margin: '5px' }}>
                        <Toolbar style={{ backgroundColor: '#70963E', color: '#fff' }}>รายการทำงาน</Toolbar>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>หัวข้อ</TableCell>
                                    <TableCell>เริ่มต้น</TableCell>
                                    <TableCell>สิ้นสุด</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {works.map(work => (
                                    <TableRow key={work._id}>
                                        <TableCell>{work.title}</TableCell>
                                        <TableCell>{moment(work.start).format('YYYY-MM-DD เวลา HH:mm น.')}</TableCell>
                                        <TableCell>{moment(work.end).format('YYYY-MM-DD เวลา HH:mm น.')}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
};

export default Home;
