// src/contexts/SpeedContext.js
import React, { createContext, useState } from 'react';

export const SpeedContext = createContext();

export const SpeedProvider = ({ children }) => {
    const [speedM1, setSpeedM1] = useState('');
    const [speedM2, setSpeedM2] = useState('');
    const [speedM3, setSpeedM3] = useState('');

    return (
        <SpeedContext.Provider value={{ speedM1, setSpeedM1, speedM2, setSpeedM2, speedM3, setSpeedM3 }}>
            {children}
        </SpeedContext.Provider>
    );
};
