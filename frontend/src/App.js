import React, { useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Appbar from './components/appbar/Appbar';
import DriveControl from './page/DriveControl';
import CNCControl from './page/CncControl';
import TransportControl from './page/TransportControl';
import Home from './page/Home';
import WorkList from './page/WorkListPage';
import Spraybar from './page/Spraybar';
import VegetablePlot from './page/VegetablePlot';

const App = () => {

  return (
    <div className="App">
      <Router>
        <Appbar />
        <div className="bg-gray-100">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/drive-control" element={<DriveControl />} />
            <Route path="/cnc-control" element={<CNCControl />} />
            <Route path="/transport-control" element={<TransportControl />} />
            <Route path="/spraybar-control" element={<Spraybar />} />
            <Route path="/work-list" element={<WorkList />} />
            <Route path="/vegetable-control" element={<VegetablePlot />} />
          </Routes></div>
      </Router>
    </div>
  );
};

export default App;
