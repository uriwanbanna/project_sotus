// src/services/plotService.js
const plots = [
    { id: 1, name: 'Plot 1', type: 'Vegetable', size: '10x10', coordinates: '10,20', status: 'Planted', crops: ['Carrot'] },
    { id: 2, name: 'Plot 2', type: 'Fruit', size: '15x15', coordinates: '20,30', status: 'Harvested', crops: [] },
  ];
  
  export const getAllPlots = () => {
    return plots;
  };
  
  export const getPlotById = (id) => {
    return plots.find(plot => plot.id === id);
  };
  
  export const addPlot = (plot) => {
    plot.id = plots.length + 1;
    plots.push(plot);
  };
  
  export const updatePlot = (id, updatedPlot) => {
    const index = plots.findIndex(plot => plot.id === id);
    if (index !== -1) {
      plots[index] = { ...plots[index], ...updatedPlot };
    }
  };
  
  export const deletePlot = (id) => {
    const index = plots.findIndex(plot => plot.id === id);
    if (index !== -1) {
      plots.splice(index, 1);
    }
  };
  
  export const searchPlots = (criteria) => {
    return plots.filter(plot => Object.keys(criteria).every(key => plot[key].includes(criteria[key])));
  };
  