import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { green, red, yellow } from '@mui/material/colors';

// สร้างธีมที่กำหนดเอง
const theme = createTheme({
  typography: {
    fontFamily: 'Prompt, sans-serif',
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
    },
  },
  palette: {
    amber: {
      main: '#ffc107',
    },
    primary: {
      main: '#212121',
      light: '#96BAFF',
      dark: '#424242',
    },
    cyan: {
      main: '#00bcd4',
    },
    secondary: {
      main: '#009688',
    },
    indigo: {
      main: '#3f51b5',
    },
    error: {
      main: red.A400,
    },
    warning: {
      main: yellow[800],
    },
    success: {
      main: green[500],
    },
    lightGreen: {
      main: '#c8e6c9',
    },
    gray: {
      main: '#9e9e9e',
    },
    background: {
      default: '#F4F5F7',
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
